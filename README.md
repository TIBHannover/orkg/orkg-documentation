# ORKG Documentation

This repository contains the documentation of the Open Research Knowledge Graph (ORKG).

The rendered output can be viewed here: https://tibhannover.gitlab.io/orkg/orkg-documentation/orkg-documentation/main/index.html.

## Building the documentation locally

It is build using [Antora](https://antora.org/) and written in [Asciidoc](https://asciidoc.org/).
To install Antora, check their [quickstart](https://docs.antora.org/antora/latest/install-and-run-quickstart/) or [installation instruction](https://docs.antora.org/antora/latest/install/install-antora/).

To build the documentation, install the dependencies via `npm install`, and then run `npx antora antora-playbook.yml`.
To fetch the lastest updates of all repositories, add the `--fetch` parameter: `npx antora antora-playbook.yml --fetch`.

## Documentation structure and style

### Structure

The project follows the [DIVIO Documentation System](https://documentation.divio.com/) structure.

ORKG component projects are recommended to follow the same structure to make integration easier.

### Style

The documentation is written [one sentence per line](https://sive.rs/1s), as dictated by common sense, and the [Asciidoc recommended practices](https://asciidoctor.org/docs/asciidoc-recommended-practices/#one-sentence-per-line).

## Antora configuration

### This repository

The project can be configured to include the documentation of ORKG components.
In fact, it is the main reason for its existance, so that the documentation can be maintained close to the code.

Component can be added to the playbook (`antora-playbook.yml`) easily.
Their pages can be referenced via Antora's [resource ID coordinates](https://docs.antora.org/antora/latest/page/resource-id-coordinates/), e.g. using the [`xref` macro](https://docs.antora.org/antora/latest/page/xref/).

This project serves as a "container" that collects and arranges information about the individual components, and acts as documentation on ORKG itself.
It uses a single [content source root](https://docs.antora.org/antora/latest/content-source-repositories/#content-source-root) at the root of this repository, with a single (root) module.

#### UI changes

The UI has been modified via the `supplemental_parts` mechanism.
Overrides can be found in the `supplemental-ui` directory.
The orginal header navbar has been replaced with a version that does not show the dummy navbar elements of the UI template.

